export default interface product {
  id: number;
  name: string;
  price: number;
}
