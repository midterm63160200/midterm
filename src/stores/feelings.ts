import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type feeling from "@/feeling";
export const usefelingStore = defineStore("feelings", () => {
  const feelings = ref<feeling[]>([
    {
      id: 1,
      name: "ANGRY",
      img: "../../../feeling_imgs/angry.png",
    },
    {
      id: 2,
      name: "CUTE",
      img: "../../../feeling_imgs/cute.png",
    },
    {
      id: 3,
      name: "EXCITED",
      img: "../../../feeling_imgs/excited.png",
    },
    {
      id: 4,
      name: "PAIN",
      img: "../../../feeling_imgs/pain.png",
    },
    {
      id: 5,
      name: "SHOCKED",
      img: "../../../feeling_imgs/shocked.png",
    },
    {
      id: 6,
      name: "SMILE",
      img: "../../../feeling_imgs/smile.png",
    },
  ]);

  return {
    feelings,
  };
});
